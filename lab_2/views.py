from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = "i am just an ordinary Computer Science student"
#"Typically used in ecommerce funnels, they can be used to describe a product or offer in sufficient detail so as to “warm up” a visitor to the point where they are closer to making a purchasing decision"

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)